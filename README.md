# Purpose

This tool is a web server to test the OIDC IDP configuration (in authorization code for the moment)

# Environment variables

* PORT: the listening port for the portal back end. Defaulted to 10088. 
* NODE_ENV: Set to `development` to activate development features and to `production` for non development environment
* EXTERNAL_OWN_URL: defines the exposing url of the application in particular when behind a reverse proxy

# Implemented endpoints

The back end implements several endpoints some of which are intended for testing and demo purpose and must be deactivated in prodution (environment variable NODE_ENV != development)

## Session endpoints

- `GET /session/login`: initiate an OIDC authentication (code flow) to the IDP at `IDENTITY_PROVIDER_BASE_URL`. Accepts a `redirect_uri` query parameter (or uses the referrer header) to redirect the user agent after a successful authentication. If a session already exists when calling this endpoint it will be used to refresh the session instead of initiating a new authentication.
- `GET /session/callback`: receives the response (code) of the OIDC authentication code flow and call the IDP `/token` endpoint to exchange the code to the tokens. Then if authentication is valid, sets the session cookies and save the refresh token in cache.
- `GET /session/logout`: clear the session in cache, clear the cookie and redirect the user agent to the logout endpoint of the IDP. After logout the user agent is redirected to the `/session/logout/end` endpoint
- `GET /session/logout/end`: Complete the logout process and clear the cookie when redirecting the user agent to a completion page. 
- `POST /session/refresh`: If a session exists and a refresh token has been saved in cache, the session is renewed and new cookies are set to the user agent. In post call, the endpoint returns an http status or 200 on success, 403 on failure and 500 on internal error. This endpoint is useful for refreshing the session from javascript calls. 
- `GET /session/refresh`: Behave like the post version, but redirect to the referer header in case of success and the login endpoint in case of failure. 
- `GET /session/state`: returns a json structure with the following 
```json
{
  "active": true, // or false if the session does not exists
  "expiresAt": 1583311509840, // unix timestamp of expiration or missing if not active
  "expiresInMs": 296425, // expiration delay until expiration or missing if not active
  "email": "test@example.com", // email of the connected user or missing if not active
  "csrfToken": "6f9783dc-352a-434d-82a1-59bafb5034bf" // csrf token to be used as header in the api call or missing if not active
}
```
- `GET /refresh/script`: Gets a js script that provides tools to refresh the session automatically when the session is about to expires. Simply calls the `autoRefreshSession()` in your HTML page to maintain the session active while your application is active in the browser.
- `GET /error?message=xxx`: designed to display an error in the authentication processes. Not implemented yet.
