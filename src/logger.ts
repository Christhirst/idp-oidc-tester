class Logger {
  debug(message: string, meta?: any): void {
    if (meta) {
      console.debug(message, meta);
    } else {
      console.debug(message);
    }
  }
  info(message: string, meta?: any): void {
    if (meta) {
      console.log(message, meta);
    } else {
      console.log(message);
    }
  }

  warn(message: string, meta?: any): void {
    if (meta) {
      console.warn(message, meta);
    } else {
      console.warn(message);
    }
  }

  error(message: string, meta?: any): void {
    if (meta) {
      console.error(message, meta);
    } else {
      console.error(message);
    }
  }
}

export default new Logger();
