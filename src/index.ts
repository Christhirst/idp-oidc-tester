// Disabling camelcase because oidc standard fields are not camelcase
/* eslint-disable @typescript-eslint/camelcase */
import dotenv from "dotenv";
dotenv.config();

import express, {
  Request,
  Response,
  NextFunction,
  RequestHandler
} from "express";

import {
  SessionMgmt,
  InvalidSession,
  NotFound,
  AlreadyExist
} from "./usecase/session-middlewares";
import CacheControl from "express-cache-controller";
import BodyParser, { urlencoded } from "body-parser";
import cookieParser from "cookie-parser";
import cors from "cors";
import logger from "./logger";
import morgan from "morgan";

import { Issuer, Client } from "openid-client";
import RedisCache from "./cache/redis";
import InMemoryCache from "./cache/in-memory";
import { CacheInterface } from "./usecase/adapter-interfaces";
import sessionRouter from "./controllers/session";
import {IdpConfiguration, NewIDPFunction} from "./usecase/idp-configuration";

let cacheProvider: CacheInterface;
if (process.env.CACHE_IN_MEMORY === "true") {
  cacheProvider = new InMemoryCache();
  logger.info("Starting with cache in memory");
} else if (process.env.REDIS_URL) {
  cacheProvider = new RedisCache(process.env.REDIS_URL, "PORTAL");
  logger.info("Starting with cache in redis", { url: process.env.REDIS_URL });
} else {
  logger.info("Starting with cache in memory since REDIS_URL is not defined");
  cacheProvider = new InMemoryCache();
}

if (!process.env.EXTERNAL_OWN_URL) {
  logger.error("Define env var ", { varName: "EXTERNAL_OWN_URL" });
  process.exit(1);
}


let idp: Issuer<Client>;
let oidcClient: Client;

/** Provides the path that will be allowed to receive the session token
 * By default `session` and `api` are provided
 */
async function servicesGetter(introspect: any): Promise<string[]> {
  type Authorization = { code: string; values: string[] };
  const authorizations: Authorization[] =
    (introspect.authorizations as Authorization[]) || [];
  const services = authorizations.find(
    auto => auto.code.toLowerCase() == "services"
  ) || {
    code: "services",
    values: []
  };
  //services.values.push("");
  return services.values.map(s => s.toLowerCase());
}

const app = express();

const isXHR = (req: Request, res: Response, next: NextFunction): void => {
  res.locals.isXHR = true;
  next();
};

const corsMw = cors({
  origin:
    process.env.NODE_ENV == "development" ? true : process.env.EXTERNAL_OWN_URL,
  credentials: true
});

const LongCacheControlMaxAge: number = Number.parseInt(
  process.env.LONG_CACHE_CONTROL || (60 * 60 * 24).toString()
);
const ShortCacheControlMaxAge: number = Number.parseInt(
  process.env.SHORT_CACHE_CONTROL || (60).toString()
);
const LongCacheControl: RequestHandler = CacheControl({
  public: true,
  maxAge: LongCacheControlMaxAge
});
const ShortCacheControl: RequestHandler = CacheControl({
  public: true,
  maxAge: ShortCacheControlMaxAge
});
const NoCache: RequestHandler = CacheControl({ private: true, noStore: true });
if (!process.env.DO_NOT_LISTEN) {
  app.use(morgan("combined"));
}
app.use(cookieParser());
app.use(BodyParser.urlencoded({ extended: true }));
app.use(BodyParser.json());
//app.use(["/js", "/css", "\*.ico", "\*.png"], ShortCacheControl)

function newIDPEvent(_idp: Issuer<Client>, _client: Client) {
  idp = _idp;
  oidcClient = _client
}
const clientOps = {
  client_id: "will be replaced",
  redirect_uris: [process.env.EXTERNAL_OWN_URL + "/session/callback"],
  post_logout_redirect_uris: [
    process.env.EXTERNAL_OWN_URL + "/session/logout/end"
  ]
}
const idpConf = new IdpConfiguration(clientOps, newIDPEvent);

app.use("/idp-config", NoCache, idpConf.mw());

const clientResolver = (req: Request)=>{
  return idpConf.getClientFromRequest(req)
}
const sessionMgmt = new SessionMgmt(clientResolver, cacheProvider, servicesGetter);

app.use(["/session/state", "/session/refresh"], isXHR);
app.use("/session", NoCache, corsMw, sessionRouter(sessionMgmt));


app.use(
  "/environment",
  isXHR,
  LongCacheControl,
  corsMw,
  (req: Request, res: Response, next: NextFunction) => {
    res.json({
      baseUrl: process.env.EXTERNAL_OWN_URL,
      accountUrl: process.env.ACCOUNT_URL
    });
  }
);

app.set('views', './views');
app.set('view engine', 'pug');

app.get('/', (req, res) => {
  const config = idpConf.getConfigFromCookie(req);
	res.render('index', {currentConfig: config?.name});
});
app.get(['/config','/config-form'], (req, res) => {
  const config = idpConf.getConfigFromCookie(req);
  res.render(req.path.substr(1), {currentConfig: config?.name, query: req.query, callback_uri: clientOps.redirect_uris[0]})
})

app.use(LongCacheControl, express.static("views/static"))

app.use((err: any, req: Request, res: Response, next: NextFunction) => {
  logger.error("Error captured:", { message: err?.message, fullError: err });
  if (err instanceof InvalidSession) {
    if (res.locals.isXHR) {
      // we have a xhr request normally
      res.status(401).json({ error: "invalid-session" });
    } else {
      req.query.redirect_uri = req.url;
      sessionMgmt.getLoginMw()(req, res, next);
    }
  } else if (err instanceof AlreadyExist) {
    if (res.locals.isXHR) {
      // we have a xhr request normally
      res.status(403).json({ message: err.message });
    } else {
      req.query.redirect_uri = req.url;
      sessionMgmt.getLoginMw()(req, res, next);
    }
  } else {
    const errorMessage = err?.message || err.toString() || "empty message";
    if (res.locals.isXHR) {
      // we have a xhr request normally
      res.status(500).json({ error: errorMessage });
    } else {
      res.render("error", {message: errorMessage});
    }
  }
});

const port: number = Number.parseInt(process.env.PORT || "10088");
if (process.env.DO_NOT_LISTEN) {
  logger.info("DO NOT LISTEN env variable activated.");
} else {
  app.listen(port, () => logger.info(`Portal backend listening`, { port: port }));
  process.on("SIGABRT", cleanTerminate);
  process.on("SIGINT", cleanTerminate);
  process.on("SIGBREAK", cleanTerminate);
}

export function closeAll(): void {
  cacheProvider.close();
}

function cleanTerminate(signal: NodeJS.Signals): void {
  logger.info("cleaning before terminating process ...", { signal: signal });
  closeAll();
  process.exit(0);
}

export default app;
