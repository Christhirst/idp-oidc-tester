import express, {
  RequestHandler,
  Request,
  Response,
  NextFunction
} from "express";
import { SessionMgmt } from "../usecase/session-middlewares";

export default function sessionRouter(
  sessionMgmt: SessionMgmt
): RequestHandler {
  const sessionRouter = express.Router();

  sessionRouter.get("/register", sessionMgmt.getRegisterMw());
  sessionRouter.get("/login", sessionMgmt.getLoginMw());
  sessionRouter.get("/callback", sessionMgmt.getCodeAuthFlowCallbackMw());
  sessionRouter.get("/logout", sessionMgmt.getLogoutMw());
  sessionRouter.get("/logout/end", sessionMgmt.getPostLogoutMw());
  sessionRouter.get("/state", sessionMgmt.getStateMw());
  sessionRouter.get("/tokens", sessionMgmt.getTokensMw());
  sessionRouter.get("/userinfo", sessionMgmt.getUserInfoMw());
  sessionRouter.post(
    "/refresh",
    sessionMgmt.getRefreshSessionMw(),
    (req: Request, res: Response, next: NextFunction) => {
      res.end();
      next();
    }
  );

  sessionRouter.get(
    "/refresh",
    sessionMgmt.getRefreshSessionMw(),
    async (req: Request, res: Response, next: NextFunction) => {
      if (res.statusCode == 200) {
        res.redirect(req.headers.referer || "/");
      } else {
        res.redirect("/session/login");
      }
      next();
    }
  );

  return sessionRouter;
}
