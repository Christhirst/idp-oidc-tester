import fs from "fs";
import express, {
  Request,
  Response,
  NextFunction,
  RequestHandler,
  Router
} from "express";
import {
  Issuer,
  Client,
  TokenSet,
  IntrospectionResponse,
  UserinfoResponse,
  OpenIDCallbackChecks,
  ClientMetadata
} from "openid-client";
import logger from "../logger";
import { json } from "body-parser";


export interface ClientInfo {
  clientId: string,
  clientSecret?: string,
  scope?: string
}
export type IssuerInfo =  {
  issuer: string,
  jwks_uri: string,
  authorization_endpoint: string,
  end_session_endpoint: string,
  token_endpoint: string,
  userinfo_endpoint: string,
  introspection_endpoint: string,
  [key: string]: any
} | string

export interface IDPConfig {
  name: string,
  client: ClientInfo,
  issuer: IssuerInfo
}


export type NewIDPFunction = (issuer:Issuer<Client>, client: Client) => void;
const IdpConfigFileName = ".idp-conf";

export class IdpConfiguration {
  private router: Router;
  private issuer: Issuer<Client>;
  private store: IDPConfig[];
  //public currentConfig: string; 
  constructor(public clientOpts: ClientMetadata, public onNewIDP: NewIDPFunction) {
    //this.issuer = defaultIssuer();
    //onNewIDP(this.issuer);
    this.loadFromFile()

    this.router=express.Router();
    this.router.get("/", this.getAllConfig.bind(this))
    this.router.get("/names", this.getAllConfigNames.bind(this))
    this.router.get("/:name/config", this.getConfigInfo.bind(this))
    this.router.put("/:name/issuer", this.setIssuer.bind(this))
    this.router.put("/:name/client", this.setClient.bind(this))
    this.router.post("/:name/apply", this.applyConfig.bind(this))

  }
  mw(): Router {
    return this.router;
  }

  loadFromFile(): void {
    if(fs.existsSync(IdpConfigFileName)) {
      this.store = JSON.parse(fs.readFileSync(IdpConfigFileName, "utf8"))
    } else {
      this.store = []
    }
  }
  saveToFile(): void {
    setImmediate(()=>{
      try {
        fs.writeFileSync(IdpConfigFileName, JSON.stringify(this.store, null, 2), "utf8")
      } catch (error) {
        logger.warn("Fail saving the config file", error)
      }
    })
  }

  getByName(name: string): IDPConfig | undefined {
    return this.store.find((config)=>config.name==name)
  }
  setPartialByName(name: string, config: Partial<IDPConfig>): IDPConfig {
    let conf = this.getByName(name)
    if(!conf) {
      conf = {
        ...config,
        name: name
      } as IDPConfig;
      this.store.push(conf)
    } else {
      Object.assign(conf, config)
    }
    this.saveToFile()
    return conf;
  }

  getAllConfig(req: Request, res: Response, next: NextFunction): void {
    const config = this.getConfigFromCookie(req);
    const currentConfig = config?.name;
    res.json(this.store.map(c=>{
      if(c.name==currentConfig) {
        return {
          ...c,
          current:true
        }
      } else {
        return c;
      }
    }))
    next()
  }
  getAllConfigNames(req: Request, res: Response, next: NextFunction): void {
    res.json(this.store.map(c=>c.name))
    next()
  }
  async getConfigInfo(req: Request, res: Response, next: NextFunction): Promise<void> {
    res.json(this.getByName(req.params.name) || {error: "No config with that name"})
    next()
  }

  async setIssuer(req: Request, res: Response, next: NextFunction): Promise<void> {
    const name = req.params.name;
    const url = req.query.url || req.body.url;
    if(url) {
      this.setPartialByName(name, {issuer:url});
    } else {
      const meta = {
        ...(this.issuer?this.issuer.metadata:{}),
        ...req.body,
      }
      this.setPartialByName(name, {issuer: {
        issuer: meta.issuer,
        authorization_endpoint: meta.authorization_endpoint , //|| meta.baseUrl + "/auth",
        token_endpoint: meta.token_endpoint, // || meta.baseUrl + "/token",
        introspection_endpoint: meta.introspection_endpoint, // || meta.baseUrl + "/token/instrospect",
        userinfo_endpoint: meta.userinfo_endpoint, // || meta.baseUrl + "/userinfo",
        jwks_uri: meta.jwks_uri, // || meta.baseUrl + "/certs",
        end_session_endpoint: meta.end_session_endpoint, // || meta.baseUrl + "/logout",
      } })
    }
    this.getConfigInfo(req, res, next)
  }
  async setClient(req: Request, res: Response, next: NextFunction): Promise<void> {
    const name = req.params.name;
    
    this.setPartialByName(name, {client: {
      clientId: req.body.client_id,
      clientSecret: req.body.client_secret,
      scope: req.body.scope
    } })
    this.getConfigInfo(req, res, next)
  }

  private setCookieConfig(res: Response, config: IDPConfig) : void {
    const base64String = Buffer.from(JSON.stringify(config), "utf-8").toString("base64");
    res.cookie("OIDCConfigData", base64String)
  }

  getConfigFromCookie(req: Request): IDPConfig | undefined {
    const base64String = req.cookies["OIDCConfigData"];
    if(base64String) {
      try {
        return JSON.parse(Buffer.from(base64String, "base64").toString("utf-8"))
      } catch (error) {
        logger.warn("Invalid base64 json data:", base64String)
        return undefined;
      }
    } else {
      return undefined;
    }
  }

  async getClientFromRequest(req: Request): Promise<Client | undefined> {
    const config = this.getConfigFromCookie(req);
    if(!config) return undefined;
    try {
      let issuer;
      let client;
      if(typeof config.issuer === "string" ) {
        issuer = await Issuer.discover(config.issuer)
      } else {
        issuer = new Issuer(config.issuer)
      }
      client = new issuer.Client({
        ...this.clientOpts,
        client_id: config.client.clientId,
        client_secret: config.client.clientSecret,
        default_scope: config.client.scope || "openid"
      })
      client.config = config.name;
      return client;
    } catch (error) {
      logger.error("Fail creating the Issuer and Client", error)
      return undefined;
    }
  }

  async applyConfig(req: Request, res: Response, next: NextFunction): Promise<void> {
    const name = req.params.name;
    const config = this.getByName(name);
    if(!config) {
      res.status(400).json({error:"undefined config name"});
      return;
    } 
    try {

      //this.onNewIDP(issuer, client)
      //this.currentConfig = name;
      this.setCookieConfig(res, config)
      res.json(config)
    } catch (error) {
      logger.error("Fail setting the configuration for "+name, error)
      res.status(400).json({error: "Impossible to apply the config. Review it first.", details: error.message})
    }
    next()
  }

}