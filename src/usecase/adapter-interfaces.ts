export interface CacheInterface {
  put(key: string, data: any, durationSec: number): Promise<boolean>;
  get(key: string): Promise<any>;
  del(key: string): Promise<boolean>;
  close(): void;
}
