import { CacheInterface } from "../usecase/adapter-interfaces";
import redis, { RedisClient } from "redis";

export default class RedisCache implements CacheInterface {
  url: string;
  prefix: string;
  client: RedisClient | null;
  reconnectTimer: NodeJS.Timer | null;
  constructor(url: string, prefix: string) {
    // create and connect redis client to local instance.
    // [redis:]//[[user][:password@]][host][:port][/db-number][?db=db-number[&password=bar[&option=value]]
    this.url = url;
    this.prefix = prefix;
    this._initialize();
  }

  private _initialize(): void {
    this.client = redis.createClient(this.url, { prefix: this.prefix + ":" });
    this.client.on("error", this._errorHandler.bind(this));
    this.reconnectTimer = null;
  }

  private _errorHandler(error: any): void {
    //console.error("REDIS-IMPL", "ERROR HANDLER", error);
    this.close();
    if (error.code == "ECONNREFUSED" || error.code == "UNCERTAIN_STATE") {
      this.reconnectTimer = setTimeout(() => this._initialize(), 1000);
    }
  }

  put(key: string, value: any, durationSec: number): Promise<boolean> {
    if (this.client) {
      const client = this.client;
      if (!value) {
        return this.del(key);
      }
      //console.log('PUT', key, value, durationSec)
      const duration = durationSec;
      return new Promise((resolve, reject) => {
        if (Number.isNaN(duration)) {
          // no duration, no expiry
          client.set(key, JSON.stringify(value));
        } else {
          client.set(key, JSON.stringify(value), "EX", durationSec);
        }
        resolve();
      });
    } else {
      throw new Error("REDIS NOT INITIALIZED");
    }
  }

  get(key: string): Promise<any> {
    if (this.client) {
      const client = this.client;
      return new Promise((resolve, reject) => {
        client.get(key, (err: any, value: string) => {
          if (err) {
            return reject(err);
          }
          resolve(JSON.parse(value));
        });
      });
    } else {
      throw new Error("REDIS NOT INITIALIZED");
    }
  }
  del(key: string): Promise<boolean> {
    if (this.client) {
      const client = this.client;
      return new Promise((resolve, reject) => {
        client.del(key, (err: any, reply: number) => {
          if (err) {
            return reject(err);
          }
          resolve(true);
        });
      });
    } else {
      throw new Error("REDIS NOT INITIALIZED");
    }
  }
  close(): void {
    //console.log("REDIS-IMPL", "CLOSING...");
    if (this.client) {
      this.client.quit();
    }
    this.client = null;
    if (this.reconnectTimer) {
      clearTimeout(this.reconnectTimer);
    }
  }
}
