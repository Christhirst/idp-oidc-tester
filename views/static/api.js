
function emptyCallback(err, data) {}
function get(path, cb) {
  if(!cb) cb = emptyCallback;
  fetch(path, {method:"GET"})
  .then(res=>{
    if(res.status==200) {
      return res.json()
    } else {
      return res.json()
    }
  })
  .then(data=>{
    if(data.error) cb(data)
    else cb(null, data)
  })
  .catch(err=>{
    console.error("Error while handling GET request "+path, err)
    cb(err)
  })
}
function post(path, body, cb) {
  if(!cb) cb = emptyCallback;
  fetch(path, {
    method:"POST", 
    headers: {"Content-Type": "application/json"}, 
    body: JSON.stringify(body)
  })
  .then(res=>{
    if(res.status==200) {
      return res.json()
    } else {
      return res.json()
    }
  })
  .then(data=>{
    if(data.error) cb(data)
    else cb(null, data)
  })
  .catch(err=>{
    console.error("Error while handling POST request "+path, err)
    cb(err)
  })
}
function put(path, body, cb) {
  if(!cb) cb = emptyCallback;
  fetch(path, {
    method:"PUT", 
    headers: {"Content-Type": "application/json"}, 
    body: JSON.stringify(body)
  })
  .then(res=>{
    if(res.status==200) {
      return res.json()
    } else {
      return res.json()
    }
  })
  .then(data=>{
    if(data.error) cb(data)
    else cb(null, data)
  })
  .catch(err=>{
    console.error("Error while handling PUT request "+path, err)
    cb(err)
  })
}

globalThis.api = {
  get: get,
  put: put,
  post: post
}