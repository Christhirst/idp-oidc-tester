
var store = {
  state: undefined,
  menus: {}
}

function initMenu() {
  store.menus["session"] = ()=>{
    loadState(()=>{
      displayCode(store.state)
    })
  }
  store.menus["accessToken"] = ()=>{
    asyncGetTokens(tokens=>{
      displayCode({
        raw: tokens.accessToken,
        introspected: tokens.decoded.accessToken
      })
    })
  }
  store.menus["idToken"] = ()=>{
    asyncGetTokens(tokens=>{
      displayCode({
        raw: tokens.idToken,
        decoded: tokens.decoded.idToken
      })
    })
  }
  store.menus["refreshToken"] = ()=>{
    asyncGetTokens(tokens=>{
      displayCode({
        raw: tokens.refreshToken,
        decoded: tokens.decoded.refreshToken
      })
    })
  }
  store.menus["userInfo"] = ()=>{
    asyncGetUserInfo(info=>{
      displayCode(info)
    })
  }
}

function displayCode(obj) {
  var code = document.getElementById("content-code")
  if(typeof obj === "string") {
    code.textContent = obj
  } else {
    code.textContent = JSON.stringify(obj, null, 2)
  }
}

function loadState(done) {
  globalThis.api.get("session/state", (err, data)=>{
    if(err) return done(err)
    store.state = data
    done()
  })
}

function asyncGetUserInfo(cb) {
  if(!store.userInfo) {
    loadUserInfo(()=>cb(store.userInfo))
  } else {
    cb(store.userInfo)
  }
}

function asyncGetTokens(cb) {
  if(!store.tokens) {
    loadTokens(()=>cb(store.tokens))
  } else {
    cb(store.tokens)
  }
}

function loadTokens(done) {
  globalThis.api.get("session/tokens", (err, data)=>{
    if(err) return done(err)
    console.log("Tokens:", data)
    store.tokens = data
    done()
  })
}

function loadUserInfo(done) {
  globalThis.api.get("session/userinfo", (err, data)=>{
    if(err) return done(err)
    console.log("UserInfo:", data)
    store.userInfo = data
    done()
  })
}

function displaySessionState() {
  displayCode(store.state)
}

function getMenuFromLocation() {
  let hash = document.location.hash
  hash = hash.substr(1) // remove "#"
  const re = /menu=(.+)/
  const r = re.exec(hash) 
  console.log(r)
  if(r && r.length==2) {
    const menu = r[1];
    return menu
  } else return undefined
}

initMenu()
const menu = getMenuFromLocation()
if(menu) {
  clickMenu(menu)
} else {
  clickMenu("session")
}
 