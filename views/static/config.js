console.log("Config script loaded")

var store = {
  configList: [],
  currentConfig: undefined,
  menus: {}
}

function initMenu() {
  function gotoHome(menu) {
    document.location.href="#menu="+menu
  }
  store.menus.default = gotoHome
}
initMenu()

function loadList(done) {
  this.api.get("idp-config", (err, list)=>{
    if(err) return;
    store.configList = list
    done()
  })
}

function createEntries() {
  var nav = document.getElementById("left-nav");
  if(!nav) return
  store.configList.forEach(config => {
    var a = document.createElement("a")
    a.setAttribute("class", "button")
    a.setAttribute("href", `javascript:displayConfig("${config.name}")`)
    a.textContent=config.name
    nav.appendChild(a)
  });
}

loadList(()=>{
  if(document.location.pathname.includes("config-form")) {
    populateForm()
  } else {
    createEntries()

    if(store.configList.length==0) {
      document.location.href="config-form"
      return;
    }
    displayConfig(store.currentConfig)
  }
})

function displayConfig(name) {
  var actions = document.getElementById("content-action")
  if(actions) {
    if(name) {
      actions.style = ""
    } else {
      actions.style = "display: none"
    }
  }
  var title = document.getElementById("content-title")
  if(title) {
    title.innerHTML = name? "Config for <b>"+name+"</b>" : "Select a configuration"
  }
  var content = document.getElementById("content-code")
  if(content) {
    var config = store.configList.find(c=>c.name==name)
    console.log("Display", name, config)
    content.textContent = JSON.stringify(config, null, 2)
    store.currentConfig = name
  }
} 

function editConfig() {
  document.location.href='config-form?name='+store.currentConfig
}

function selectConfig() {
  if(!store.currentConfig) return
  globalThis.api.post(`idp-config/${store.currentConfig}/apply`, {}, (err, data)=>{
    console.log("Select config: ", err, data)
    if(err) {
      document.location.href="error?message="+err.error+"&details="+JSON.stringify(err.details)
    } else {
      document.location.href="."
    }
  })
}
function setIdValue(id, value) {
  var e = document.getElementById(id)
  if(e) e.setAttribute("value", value)
}
function getIdValue(id) {
  var e = document.getElementById(id)
  if(e) return e.value
  else return undefined
}
function populateForm() {
  if(!document.getElementById("form")) return
  if(store.currentConfig) {
    setIdValue("name", store.currentConfig)
    document.getElementById("name").setAttribute("readonly", "true")
    var config = store.configList.find(c=>c.name==store.currentConfig)
    if(!config) return
    if(typeof config.issuer === "string") {
      setIdValue("url", config.issuer)
    } else {
      setIdValue("issuer", config.issuer.issuer)
      setIdValue("baseUrl", config.issuer.baseUrl)
      setIdValue("authorization_endpoint", config.issuer.authorization_endpoint)
      setIdValue("token_endpoint", config.issuer.token_endpoint)
      setIdValue("introspection_endpoint", config.issuer.introspection_endpoint)
      setIdValue("jwks_uri", config.issuer.jwks_uri)
      setIdValue("end_session_endpoint", config.issuer.end_session_endpoint)
      setIdValue("userinfo_endpoint", config.issuer.userinfo_endpoint)
    }
    setIdValue("client_id", config.client.clientId)
    setIdValue("client_secret", config.client.clientSecret || "")
    setIdValue("scope", config.client.scope || "")

  }
}

function saveConfigForm() {
  const name = getIdValue("name");
  globalThis.api.put(`idp-config/${name}/issuer`, {
    url: getIdValue("url"),
    issuer: getIdValue("issuer"),
    baseUrl: getIdValue("baseUrl"),
    jwks_uri: getIdValue("jwks_uri"),
    authorization_endpoint: getIdValue("authorization_endpoint"),
    end_session_endpoint: getIdValue("end_session_endpoint"),
    token_endpoint: getIdValue("token_endpoint"),
    userinfo_endpoint: getIdValue("userinfo_endpoint"),
    introspection_endpoint: getIdValue("introspection_endpoint")
  }, (err, data) => {
    console.log("Completed setting issuer", err, data)
    
    globalThis.api.put(`idp-config/${name}/client`, {
      client_id: getIdValue("client_id"),
      client_secret: getIdValue("client_secret"),
      scope: getIdValue("scope")
    }, (err, data) => {
      console.log("Completed setting client", err, data)

      if(name == store.currentConfig) {
        selectConfig()
      } else {
        document.location.href = "config"
      }

    })
  })
}